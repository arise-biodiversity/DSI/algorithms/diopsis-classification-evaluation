import ast
import os
import tempfile
import unittest
import numpy as np
import pandas

from evaluate_results import evaluate, read_name_to_ancestors


class TestEvaluation(unittest.TestCase):
    def test_fully_correct(self):
        """
        Tests a perfect prediction result
        """

        true_train = pandas.read_csv("classification_labels.csv")
        name_to_ancestors, num_ancestors_per_label = read_name_to_ancestors()
        with tempfile.TemporaryDirectory() as temp_data_directory:
            true_train = true_train.sample(frac=0.10, random_state=42)
            out_rows = []
            out_header = ["image_uid"]
            for n_ in range(6):
                out_header.append(f"level_{n_}")
                out_header.append(f"level_{n_}_probability")
            for r, row in true_train.iterrows():
                names = sorted(
                    ast.literal_eval(row.names),
                    key=lambda name_: num_ancestors_per_label[name_],
                )
                out_row = [row.basename]
                for n_ in range(6):
                    out_row.append(names[n_] if n_ < len(names) else None)
                    out_row.append(1.0 if n_ < len(names) else None)
                out_rows.append(out_row)
            out_df = pandas.DataFrame(data=out_rows, columns=out_header)
            true_csv_path = os.path.join(temp_data_directory, "true.csv")
            out_df.to_csv(true_csv_path, index=False)
            out_stats_path = os.path.join(temp_data_directory, "out_stats.csv")
            evaluate(
                true_csv_path,
                true_csv_path,
                "classification_labels.csv",
                out_stats_path,
            )
            out_stats = pandas.read_csv(out_stats_path)
            self.assertEqual(1.0, self.get_statistic_by_name(out_stats, "accuracy"))
            self.assertEqual(
                1.0, self.get_statistic_by_name(out_stats, "average_recall")
            )
            self.assertEqual(
                0.0, self.get_statistic_by_name(out_stats, "too_general_percentage")
            )

    def test_too_general(self):
        """
        Tests a non-perfect prediction result by randomly dropping true (leaf) labels
        """
        true_train = pandas.read_csv("classification_labels.csv")
        name_to_ancestors, num_ancestors_per_label = read_name_to_ancestors()
        with tempfile.TemporaryDirectory() as temp_data_directory:
            true_train = true_train.sample(frac=0.10, random_state=42)
            true_out_rows = []
            predicted_out_rows = []
            out_header = ["image_uid"]
            for n_ in range(6):
                out_header.append(f"level_{n_}")
                out_header.append(f"level_{n_}_probability")
            for r, row in true_train.iterrows():
                names = sorted(
                    ast.literal_eval(row.names),
                    key=lambda name_: num_ancestors_per_label[name_],
                )
                true_out_row = [row.basename]
                predicted_out_row = [row.basename]
                drop = False
                for n_ in range(6):
                    true_out_row.append(names[n_] if n_ < len(names) else None)
                    true_out_row.append(1.0 if n_ < len(names) else None)
                    #
                    drop = drop or ((n_ > 0) and (np.random.rand() > 0.75))
                    predicted_out_row.append(
                        names[n_] if n_ < len(names) and not drop else None
                    )
                    predicted_out_row.append(
                        1.0 if n_ < len(names) and not drop else None
                    )
                true_out_rows.append(true_out_row)
                predicted_out_rows.append(predicted_out_row)
            true_out_df = pandas.DataFrame(data=true_out_rows, columns=out_header)
            predicted_out_df = pandas.DataFrame(
                data=predicted_out_rows, columns=out_header
            )
            true_csv_path = os.path.join(temp_data_directory, "true.csv")
            true_out_df.to_csv(true_csv_path, index=False)
            predicted_csv_path = os.path.join(temp_data_directory, "predicted.csv")
            predicted_out_df.to_csv(predicted_csv_path, index=False)
            out_stats_path = os.path.join(temp_data_directory, "out_stats.csv")
            evaluate(
                true_csv_path,
                predicted_csv_path,
                "classification_labels.csv",
                out_stats_path,
            )
            out_stats = pandas.read_csv(out_stats_path)
            self.assertLess(self.get_statistic_by_name(out_stats, "accuracy"), 1.0)
            self.assertLess(
                self.get_statistic_by_name(out_stats, "average_recall"), 1.0
            )
            self.assertGreater(
                self.get_statistic_by_name(out_stats, "too_general_percentage"), 0.0
            )

    @staticmethod
    def get_statistic_by_name(out_stats, name) -> float:
        return out_stats[out_stats.statistic == name].value.values[0]
