from typing import Dict, List, Tuple


def compute_taxon_distance(
    name_to_ancestors: Dict[str, List[str]], predicted_label: str, true_label: str
) -> Tuple[str, int]:
    true_ancestors = name_to_ancestors[true_label]
    predicted_ancestors = name_to_ancestors[predicted_label]
    result = ("correct", 0)
    if predicted_label != true_label:
        if predicted_label in true_ancestors:
            result = ("too_general", true_ancestors.index(predicted_label))
        elif true_label in predicted_ancestors:
            result = ("too_specific", predicted_ancestors.index(true_label))
        else:
            common_ancestors = set(true_ancestors).intersection(predicted_ancestors)
            lowest_common_ancestor = predicted_ancestors[
                min(predicted_ancestors.index(l_) for l_ in common_ancestors)
            ]
            result = (
                "other_branch",
                predicted_ancestors.index(lowest_common_ancestor)
                + true_ancestors.index(lowest_common_ancestor),
            )
    return result
