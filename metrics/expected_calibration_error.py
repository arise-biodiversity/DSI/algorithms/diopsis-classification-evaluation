import numpy as np
import pandas
from sklearn.metrics import accuracy_score


def compute_ece(
    table: pandas.DataFrame,
    half_bin_width=0.025,
    start=0.0,
    probability_name="probability",
    label_true_name="label_true",
    label_predicted_name="label_predicted",
):
    bin_centers = np.arange(
        start + half_bin_width, 1 + half_bin_width, 2 * half_bin_width
    )
    scores = []
    num = []
    has_data = []
    for bin_center in bin_centers:
        selection = table[
            np.logical_and(
                bin_center - half_bin_width < table[probability_name],
                table[probability_name] < bin_center + half_bin_width,
            )
        ]

        scores.append(
            accuracy_score(
                selection[label_true_name].values,
                selection[label_predicted_name].values,
            )
            if len(selection) > 0
            else 0
        )
        has_data.append(len(selection) > 0)
        num.append(len(selection))

    errors = np.array(num) * np.abs(np.array(scores) - np.array(bin_centers))
    ece = errors.sum() / len(table)
    last_bin_ece = errors[-1] / num[-1]
    #
    return (
        ece,
        bin_centers,
        half_bin_width,
        num,
        scores,
        last_bin_ece,
        has_data,
    )
