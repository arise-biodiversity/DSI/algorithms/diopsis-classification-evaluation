import argparse
import ast
import os
from collections import Counter, defaultdict
from typing import Set, Dict, List, Tuple

import numpy as np
import pandas
from sklearn.metrics import accuracy_score, precision_recall_fscore_support

from metrics.expected_calibration_error import compute_ece
from metrics.taxon_distance import compute_taxon_distance


def get_leaf_label(leaf_cache: Dict[str, str], ancestor_map, _, label_list):
    cache_key = ",".join(sorted([label_ for label_ in label_list]))
    if cache_key in leaf_cache:
        return leaf_cache[cache_key]
    deepest_level = max([ancestor_map[label_] for label_ in label_list])
    deepest_label = [
        label_ for label_ in label_list if ancestor_map[label_] == deepest_level
    ][0]
    leaf_cache[cache_key] = deepest_label
    return deepest_label


def tmp_sort_by_rank(annotations: List[Tuple[str, str]], num_ancestors_per_label):
    return sorted(annotations, key=lambda x_: num_ancestors_per_label[x_[0]])


def evaluate(
    true_csv_path,
    predicted_csv_path,
    true_train_csv_path,
    out_statistics_path,
    min_prob=0.00,
):

    bins = [(1, 5), (6, 10), (11, 20), (21, 50), (51, 100), (100, 1000000)]
    bin_index_to_labels: Dict[int, Set[str]] = defaultdict(lambda: set())
    train_counts = Counter(pandas.read_csv(true_train_csv_path).deepest_name)

    for label, count in train_counts.most_common():
        for bin_index, (bin_start, bin_end) in enumerate(bins):
            if bin_start <= count <= bin_end:
                bin_index_to_labels[bin_index].add(label)

    name_to_ancestors, _ = read_name_to_ancestors()

    true_leaf_labels = []
    predicted_leaf_labels = []
    predicted_label_prob = []

    num_ancestors_per_label_all = {k_: len(v_) for k_, v_ in name_to_ancestors.items()}
    leaf_cache = {}

    gt_dataset = pandas.read_csv(true_csv_path)
    predicted_dataset = pandas.read_csv(predicted_csv_path)

    if len(gt_dataset) != len(predicted_dataset):
        print("True dataset and predicted dataset should have same length")
        exit(1)
    if not np.all(gt_dataset.image_uid.values == predicted_dataset.image_uid.values):
        print(
            "True dataset and predicted dataset should have same ordering of image_uid's"
        )
        exit(1)

    # Iterate over each dataset item, and collect the labels for this item (predicted and true)
    too_specific = []
    too_general = []
    other_branch = []
    for gt_item, predicted_item in zip(gt_dataset.values, predicted_dataset.values):
        gt_item = [
            gt_item[i_ + 1 : i_ + 3].tolist()
            for i_ in range(0, int((len(gt_item) - 1)), 2)
            if isinstance(gt_item[i_ + 1], str)
        ]
        predicted_item = [
            predicted_item[i_ + 1 : i_ + 3].tolist()
            for i_ in range(0, int((len(predicted_item) - 1)), 2)
            if isinstance(predicted_item[i_ + 1], str)
        ]
        true_leaf_label = get_leaf_label(
            leaf_cache,
            num_ancestors_per_label_all,
            None,
            [t_[0] for t_ in gt_item],
        )
        true_leaf_labels.append(true_leaf_label)
        predicted_labels = []
        probs = []
        predicted_labels_org = predicted_item
        for t_ in predicted_labels_org:
            t_.append(num_ancestors_per_label_all[t_[0]])
        predicted_labels_org = sorted(predicted_labels_org, key=lambda t_: t_[2])
        for t_ in predicted_labels_org:
            if t_[1] >= min_prob:
                predicted_labels.append(t_[0])
                probs.append(t_[1])
            else:
                break

        predicted_leaf_label = get_leaf_label(
            leaf_cache,
            num_ancestors_per_label_all,
            None,
            predicted_labels,
        )
        predicted_leaf_labels.append(predicted_leaf_label)
        predicted_label_prob.append(
            min(probs)
        )  # TODO: does not necessarily correspond to leaf label, but usually does
        true_label = true_leaf_label
        predicted_label = predicted_leaf_label

        distance_type, distance = compute_taxon_distance(
            name_to_ancestors, predicted_label, true_label
        )

        if distance_type == "too_general":
            too_general.append((true_label, predicted_label, distance))
        elif distance_type == "too_specific":
            too_specific.append((true_label, predicted_label, distance))
        elif distance_type == "other_branch":
            other_branch.append((true_label, predicted_label, distance))

    out_stats_table = []

    for d in range(1, 10):
        tmp_ = len([x for x in too_general if x[2] == d])
        if tmp_ > 0:
            print(
                f"% too general - distance = {d}:", tmp_ / float(len(predicted_dataset))
            )
    for d in range(1, 10):
        tmp_ = len([x for x in too_specific if x[2] == d])
        if tmp_ > 0:
            print(
                f"% too specific - distance = {d}:",
                tmp_ / float(len(predicted_dataset)),
            )
    for d in range(1, 10):
        tmp_ = len([x for x in other_branch if x[2] == d])
        if tmp_ > 0:
            print(
                f"% other_branch - distance = {d}:",
                tmp_ / float(len(predicted_dataset)),
            )

    y_true = true_leaf_labels
    y_predicted = predicted_leaf_labels
    labels = list(set(y_true).union(set(y_predicted)))
    label_names = [label_ for label_ in labels]
    mean_accuracy = accuracy_score(y_true, y_predicted)
    out_stats_table.append(("accuracy", mean_accuracy))
    precision, recall, fscore, support = precision_recall_fscore_support(
        y_true,
        y_predicted,
        labels=label_names,
    )
    # TODO: write to CSV
    table_per_label = []
    for i, name in enumerate(label_names):
        table_per_label.append((name, precision[i], recall[i], fscore[i], support[i]))

    table_per_label = pandas.DataFrame(
        data=table_per_label,
        columns=["label", "precision", "recall", "fscore", "num_test"],
    )

    out_stats_table.append(
        (
            f"average_recall",
            np.mean(np.mean(recall)),
        )
    )
    out_stats_table.append(
        (
            f"average_precision",
            np.mean(np.mean(precision)),
        )
    )
    out_stats_table.append(
        (
            f"average_f1",
            np.mean(np.mean(fscore)),
        )
    )

    too_general_percentage = np.round(
        len(too_general) / float(len(predicted_dataset)), 4
    )
    out_stats_table.append(("too_general_percentage", too_general_percentage))
    too_specific_percentage = np.round(
        len(too_specific) / float(len(predicted_dataset)), 4
    )
    out_stats_table.append(("too_specific_percentage", too_specific_percentage))
    other_branch_percentage = np.round(
        len(other_branch) / float(len(predicted_dataset)), 4
    )
    out_stats_table.append(("other_branch_percentage", other_branch_percentage))
    out_stats_table.append(
        (
            "taxon_distance_overall",
            np.mean([x_[2] for x_ in (too_general + too_specific + other_branch)]),
        )
    )
    out_stats_table.append(
        ("taxon_distance_too_general", np.mean([x_[2] for x_ in too_general]))
    )
    out_stats_table.append(
        ("taxon_distance_too_specific", np.mean([x_[2] for x_ in too_specific]))
    )
    out_stats_table.append(
        ("taxon_distance_other_branch", np.mean([x_[2] for x_ in other_branch]))
    )

    (
        ece,
        bin_centers,
        half_bin_width,
        num,
        scores,
        last_bin_ece,
        has_data,
    ) = compute_ece(
        pandas.DataFrame(
            data=zip(y_true, y_predicted, predicted_label_prob),
            columns=["label_true", "label_predicted", "probability"],
        )
    )
    out_stats_table.append(("expected_calibration_error", ece))
    out_stats_table.append(
        (
            f"{bin_centers[-1]-half_bin_width:.2f}-{bin_centers[-1]+half_bin_width:.2f}_expected_calibration_error",
            last_bin_ece,
        )
    )

    import matplotlib.pyplot as plt

    plt.scatter([train_counts[name_] for name_ in label_names], recall * 100)
    plt.xscale("log")
    plt.xlim(0.9, 1e4)
    plt.xlabel("Number of examples in training set")
    plt.ylabel("Recall %")
    plt.savefig("train_counts_vs_recall.png")

    for bin_index, labels in bin_index_to_labels.items():
        selection = table_per_label[table_per_label.label.isin(labels)]
        bin_start, bin_end = bins[bin_index]
        out_stats_table.append(
            (
                f"{bin_start}-{bin_end}_train_examples_average_recall",
                np.mean(selection.recall),
            )
        )
        out_stats_table.append(
            (
                f"{bin_start}-{bin_end}_train_examples_average_precision",
                np.mean(selection.precision),
            )
        )
        out_stats_table.append(
            (
                f"{bin_start}-{bin_end}_train_examples_average_f1",
                np.mean(selection.fscore),
            )
        )

    out_stats = pandas.DataFrame(data=out_stats_table, columns=["statistic", "value"])
    out_stats["value"] = out_stats["value"].round(4)
    out_stats.to_csv(out_statistics_path, index=False)

    for _, row in out_stats.iterrows():
        print(row.values)


def read_name_to_ancestors():
    name_to_ancestors = pandas.read_csv(
        os.path.join(os.path.dirname(__file__), "name_to_ancestors.csv")
    )
    name_to_ancestors = dict(
        list(
            zip(
                name_to_ancestors.name,
                [ast.literal_eval(x_) for x_ in name_to_ancestors.ancestors.values],
            )
        )
    )
    num_ancestors_per_label = {k_: len(v_) for k_, v_ in name_to_ancestors.items()}
    return name_to_ancestors, num_ancestors_per_label


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Computes statistics for the classification tasks of the DIOPSIS challenge. See "
        "http://docs.google.com/document/d/1LBPz2MkcgNyzUZ729veDz3XgjhpkL1hMeBDFhfunfXY"
        " for explanation of the challenge and file formats",
        epilog="Example usage:"
        "python evaluate_results.py true.csv predicted.csv classification_labels.csv out_stats.csv",
    )
    parser.set_defaults(func=lambda x: parser.print_usage())
    parser.add_argument(
        "true_csv_path", help="CSV containing the truth for the results"
    )
    parser.add_argument(
        "predicted_csv_path", help="CSV containing the truth for the dataset"
    )
    parser.add_argument(
        "true_train_csv_path",
        help="CSV containing the predictions for the training dataset."
        "This is used to compute statistics for the training set",
    )
    parser.add_argument(
        "out_path", help="Output file where performance statistics are written to"
    )
    args = parser.parse_args()
    evaluate(
        true_csv_path=args.true_csv_path,
        predicted_csv_path=args.predicted_csv_path,
        true_train_csv_path=args.true_train_csv_path,
        out_statistics_path=args.out_path,
    )
